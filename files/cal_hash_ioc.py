#!/bin/env python
# managed by puppet
import argparse
import json
import sys
import os

from ligo_softioc import SoftIOC

cal_id = 0
cal_hash = 0

save_file = ""

def process(ioc, time):
    """
    If either the id or hash has changed, save them.
    :param ioc:
    :param time:
    :return:
    """
    global cal_id, cal_hash, save_file
    new_id = ioc.getParam("ID_INT")
    new_hash = ioc.getParam("HASH_INT")
    if new_id != cal_id or new_hash != cal_hash:
        cal_hash = new_hash
        cal_id = new_id
        try:
            with open(save_file, "wt") as f:
                d = {
                    'cal_hash': cal_hash,
                    'cal_id': cal_id
                }
                json.dump(d, f)
        except:
          sys.stderr.write("Failed to save calibration id and hash\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='cal_hash_ioc',
        description='Store and serve the calibration ID and hash.'
    )

    parser.add_argument('save_file', default="", nargs="?")
    parser.add_argument('-i', '--print_ini', action='store_true', help="print a reasonable INI file")
    args = parser.parse_args()

    save_file = args.save_file
    print_ini = args.print_ini

    try:
        with open(save_file, "rt") as f:
            saved_vals = json.load(f)
            cal_hash = int(saved_vals.get('cal_hash', 0))
            cal_id = int(saved_vals.get('cal_id', 0))
    except:
        pass

    # get IFO
    if 'IFO' not in os.environ:
        raise Exception("Please define IFO in the environment, e.g. 'IFO=X1'")

    ifo = os.environ['IFO']

    ioc = SoftIOC(
        prefix = f"{ifo}:CAL-CALIB_REPORT_",
        process_period_sec=0.5,
        process_func=process
    )

    channels = {
        "ID_INT": {'type': 'int'},
        "HASH_INT": {'type': 'int'},
    }

    ioc.add_channels(channels)

    ioc.finalize_channels()

    ioc.setParam("ID_INT", cal_id)
    ioc.setParam("HASH_INT", cal_hash)

    if print_ini:
        ioc.print_ini()
    else:
        ioc.start()
