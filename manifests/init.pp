class cal_hash_ioc
  (
    String $user,
    String $base_dir,
    String $ifo,
  )
  {
    # install epics base

    # install pip

    # establish base directory
    file { $base_dir:
      ensure => directory,
      owner  => $user,
      group  => $user,
    }


    # create virtual env
    $venv_script = "${base_dir}/create_venv"
    $venv = "${base_dir}/venv"

    file{$venv_script:
      owner => $user,
      group => $user,
      mode => '0755',
      notify => Exec['create_venv'],
      content => @("EOF"/L)
      #!/bin/bash
      # This file is managed by puppet

      export EPICS_HOST_ARCH=linux-x86_64
      export EPICS_BASE=/usr/lib/epics
      # together these variable lead to the EPICS libraries
      # such as /usr/lib/epics/lib/linux-x86_65/libgdd.a
      # necessary to install pcaspy 0.7.3, the latest supported version in LHO CDS.

      python3 -m venv ${venv}
      source ${venv}/bin/activate

      pip install ligo-softioc
      | EOF
    }

    exec { 'create_venv':
      command => $venv_script,
      user    => $user,
      creates => $venv,
      require => File[$venv_script],
    }

    # IOC file
    $ioc_file = "${base_dir}/cal_hash_ioc.py"

    file{$ioc_file:
      owner => $user,
      group => $user,
      mode => '0755',
      source => 'puppet:///modules/cal_hash_ioc/cal_hash_ioc.py',
    }


    # create INI file
    $ini_file = "${base_dir}/${ifo}EPICS_CAL_HASH.ini"

    $create_ini_script = "${base_dir}/create_ini"

    file { $create_ini_script:
      owner   => $user,
      group   => $user,
      mode    => '0755',
      content => @("EOF"/L)
      #!/bin/bash
      # managed by puppet
      source ${venv}/bin/activate
      export IFO=${ifo}
      python3 ${ioc_file} -i > $ini_file
      | EOF
    }

    exec { 'create_ini':
      user => $user,
      command => $create_ini_script,
      creates => $ini_file,
      require => [File[$create_ini_script], Exec['create_venv']],
    }

    # setup service
    $run_script = "${base_dir}/run_cal_hash_ioc"

    file {$run_script:
      owner   => $user,
      group   => $user,
      mode    => '0755',
      content => @("EOF"/L)
      #!/bin/bash
      # managed by puppet
      source ${venv}/bin/activate
      export IFO=${ifo}
      python3 ${ioc_file} $@
      | EOF
     }

    $save_file="${base_dir}/save.json"
    $systemd_file='/etc/systemd/system/cal_hash_ioc.service'
    file {$systemd_file:
      content => @("ENDOFFILE")
      [Unit]
      Description="Store and provide latest calibration ID number and Hash"

      [Service]
      ExecStart=${run_script} ${save_file}
      User=${user}
      EnvironmentFile=/etc/environment
      Restart=always
      RestartSec=60

      [Install]
      WantedBy=multi-user.target
      | ENDOFFILE
    ,
    notify => [Exec['daemon_reload'], Service['cal_hash_ioc'],],
    }

    # enable and start service

    service { 'cal_hash_ioc':
      require => [File[$systemd_file], Exec['daemon_reload'], User['ioc'], ],
      ensure  => running,
      enable  => true,
    }

}